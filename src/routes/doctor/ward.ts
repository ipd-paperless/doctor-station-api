import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';
import { WardModel } from '../../models/ward';
import { AdmitModel } from '../../models/admit';

// schema
// import createSchema from '../../schema/admit/create';
// import { HttpStatusCode } from 'axios';


export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const wardModel = new WardModel();
  const admitModel = new AdmitModel();

  // get ward
  // doctor/ward
  fastify.get('/', {
    preHandler: [
      fastify.guard.role(['admin', 'nurse', 'doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const query: any = request.query
      const { limit, offset, search } = query;
      const _limit = limit || 20;
      const _offset = offset || 0;
      const data: any = await wardModel.getWard(db, search, _limit, _offset)
      const total: any = await wardModel.getTotalWard(db, search)
      return reply.status(StatusCodes.OK)
        .send({
          ok: true,
          data,
          total: Number(total[0].count)
        });

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error);
    }
  })

  done();
}