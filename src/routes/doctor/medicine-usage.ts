import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';

// schema
import createSchema from '../../schema/admit/create';
import { HttpStatusCode } from 'axios';
import { MedicineUsageModel } from '../../models/medicine-usage';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const medicineUsageModel = new MedicineUsageModel();

  // get medicine usage
  // /doctor/medicine-usage
  fastify.get('/', {
    preHandler: [
      fastify.guard.role(['admin', 'nurse', 'doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const _query: any = request.query;
    const { limit, offset, search } = _query;
    const _limit = limit || 20;_
    const _offset = offset || 0;

    try {
      const data: any = await medicineUsageModel.getAll(db, search, _limit, _offset);
      const total: any = await medicineUsageModel.getTotal(db, search);
      return reply.status(StatusCodes.OK).
        send({
          ok: true,
          data,
          total: Number(total.total)
        });
    } catch (error: any) {
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        });
    }
  })

  done();
}