import { Knex } from 'knex';
import { UUID } from 'crypto';

export class NurseNoteModel {
  constructor () { }

  create(db: Knex, data: any) {
    return db('nurse_note')
    .insert(data);
  }

  update(db: Knex, data: any, nurseNoteID: UUID) {
    return db('nurse_note')
    .where('id', nurseNoteID)
    .update(data);
  }

  delete(db: Knex, nurseNoteID: UUID) {
    return db('nurse_note')
    .where('id', nurseNoteID)
    .delete();
  }

  upsert(db: Knex, data: any) {
    return db('nurse_note')
    .insert(data)
    .onConflict('id')
    .merge();
  }

  getByAdmitID(db: Knex, admitID: UUID) {
    return db('nurse_note')
    .where('admit_id', admitID)
    .andWhere('is_active', true)
    .select(
      'id', 'admit_id', 'nurse_note_date', 'nurse_note_time',
      'problem_list', 'activity', 'evaluate'
    )
  }

}