import { Knex } from 'knex';
import { UUID } from 'crypto';

export class OrderModel {

  /**
   * 
   * @param db 
   * @param data 
   * @returns 
   */
  create(db: Knex, data: any) {
    return db('orders')
    .returning('id')
    .insert(data)
    .returning('*');
  }

  /**
   * 
   * @param db 
   * @param data 
   * @param orderID 
   * @returns 
   */
  update(db: Knex, data: any, orderID: UUID) {
    return db('orders')
    .where('id', orderID)
    .update(data)
    .returning('*');
  }

  /**
   * 
   * @param db 
   * @param orderID 
   * @returns 
   */
  delete(db: Knex, orderID: UUID) {
    return db('orders')
    .where('id', orderID)
    .delete();
  }

  /**
   * 
   * @param db 
   * @param data 
   * @returns 
   */
  upsert(db: Knex, data: any) {
    return db('orders')
      .insert(data)
      .onConflict('id')
      .merge()
      .returning('*');
  }

  /**
   * 
   * @param db 
   * @param doctorOrderID 
   * @returns 
   */
  getByDoctorOrderID(db: Knex, doctorOrderID: UUID) {
    return db('orders')
      .where('doctor_order_id', doctorOrderID)
      .select(
        'id', 'doctor_order_id',
        'order_type_id', 'item_type_id', 'item_id', 'item_name',
        'medicine_usage_code', 'medicine_usage_extra',
        'quantity',
        'is_confirm', 'confirm_by', 'confirm_date', 'confirm_time',
        'is_process', 'process_by'

      )
  }
}