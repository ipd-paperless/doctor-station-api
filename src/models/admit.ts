import { Knex } from 'knex';
import { UUID } from 'crypto';

export class AdmitModel {

  /**
   * 
   * @param db 
   * @param doctorID 
   * @returns 
   */
  getByDoctorID(db: Knex, doctorID: UUID) {
    return db('admit')
      .where('doctor_id', doctorID)
      .select(
        'id', 'hn', 'vn', 'admit_date', 'admit_time',
        'department_id', 'ward_id', 'bed_id',
        'bed_number', 'doctor_id', 'pre_diag',
        'inscl', 'inscl_name',
        'is_active'
      )
  }

  /**
   * 
   * @param db 
   * @param admitID 
   * @returns 
   */
  getByID(db: Knex, id: UUID) {
    return db('admit as a')
      .innerJoin('patient as p', 'a.id', 'p.admit_id')
      .where('a.id', id)
      .andWhere('a.is_active', true)
      .select('a.*')
      .first();
  }

  /**
   * 
   * @param db 
   * @param wardID 
   * @param query 
   * @param limit 
   * @param offset 
   * @returns 
   */
  getAdmitByWardID(db: Knex, wardID: UUID, query: any, limit: number, offset: number) {
    let sql = db('admit as a')
      .leftJoin('patient as pt', 'a.id', 'pt.admit_id')
      .leftJoin('profile as pf', 'a.doctor_id', 'pf.user_id')
      .leftJoin('bed as b', 'a.bed_id', 'b.id')
      .select(
        'a.id as admit_id', 'a.an', 'a.hn', 'a.bed_number', 'a.doctor_id',
        'pt.title', 'pt.fname', 'pt.lname', 'pt.gender', 'pt.dob', 'pt.age',
        'pf.title as doctor_title', 'pf.fname as doctor_fname', 'pf.lname as doctor_lname',
        'b.name'
      )
      .where('a.ward_id', wardID)
      .andWhere('a.is_active', true)
    if (query) {
      let _query = `%${query}%`
      sql.where(builder => {
        builder.whereRaw('LOWER(pt.fname) like LOWER(?)', [_query])
          .orWhereRaw('LOWER(pt.lname) like LOWER(?)', [_query])
          .orWhereRaw('LOWER(pt.hn) like LOWER(?)', [_query])
      })
    }

    return sql.limit(limit).offset(offset);
  }

  /**
   * 
   * @param db 
   * @param wardId 
   * @returns 
   */
  getTotalAdmitWard(db: Knex, wardID: UUID, query: any) {
    let sql = db('admit as a')
      .leftJoin('patient as pt', 'a.id', 'pt.admit_id')
      .where('a.ward_id', wardID)
      .andWhere('a.is_active', true)
    if (query) {
      let _query = `%${query}%`
      sql.where(builder => {
        builder.whereRaw('LOWER(pt.fname) like LOWER(?)', [_query])
          .orWhereRaw('LOWER(pt.lname) like LOWER(?)', [_query])
          .orWhereRaw('LOWER(pt.hn) like LOWER(?)', [_query])
      })
    }

    return sql.count('a.id')
  }

  /**
   * บันทึกข้อมูล Admit
   * @param db 
   * @param data 
   * @returns 
   */
  create(db: Knex, data: object) {
    return db('admit')
      .insert(data);
  }

  /**
   * 
   * @param db 
   * @param data 
   * @param id 
   * @returns 
   */
  update(db: Knex, data: object, id: UUID) {
    let sql: any = db('admit')
      .where('id', id)
      .update(data)

    return sql
  }

  delete(db: Knex, id: UUID) {
    let sql: any = db('admit')
      .where('id', id)
      .delete();
  }

  info(db: Knex, id: UUID) {
    let sql = db('admit as a')
      .leftJoin('patient as p', 'a.an', 'p.an')
      .leftJoin('profile as pf', 'a.doctor_id', 'pf.user_id')
      .leftJoin('opd_review as o', 'a.id', 'o.admit_id')
      .select(
        'a.id', 'a.doctor_id', 'a.bed_number', 'a.admit_by',
        'p.an', 'p.hn', 'p.title', 'p.fname', 'p.lname', 'p.gender',
        'p.age', 'p.address', 'p.phone', 'p.inscl_name',
        'o.chief_compaint', 'o.present_illness', 'o.past_history', 'o.physical_exam',
        'o.body_temperature', 'o.body_weight', 'o.body_height', 'o.waist', 'o.pulse_rate', 'o.respiratory_rate',
        'o.systolic_blood_pressure', 'o.diatolic_blood_pressure',
        'o.oxygen_sat', 'o.eye_score', 'o.movement_score', 'o.verbal_score'
      )
      .where('a.id', id)
      .andWhere('a.is_active', true)
    return sql
  }

  // listByWardID(db: Knex, wardID:UUID) {
  //   let sql = db('admit as a')
  //     .leftJoin('patient as p', 'a.an','p.an')
  //     .leftJoin('opd_review as o', 'a.id','o.admit_id')
  //     .leftJoin('opd_treatment as t', 'a.id','t.admit_id')
  //     .select(
  //       'a.an', 'a.hn',
  //       'p.title', 'p.fname', 'p.lname','p.gender', 
  //       'p.age', 'p.inscl_name',
  //       'o.chief_compaint', 'o.present_illness', 'o.past_history', 'o.physical_exam', 
  //       'o.body_temperature', 'o.body_weight','o.body_height', 'o.waist', 'o.pulse_rate', 'o.respiratory_rate', 
  //       'o.systolic_blood_pressure', 'o.diatolic_blood_pressure', 
  //       'o.oxygen_sat', 'o.eye_score', 'o.movement_score', 'o.verbal_score'
  //     )
  //     .where('a.ward_id', wardID);
  //     return sql
  // }

  // admitByID(db: Knex, id: UUID) {
  //   let sql = db('admit as a')
  //     .leftJoin('patient as p', 'a.an', 'p.an')
  //     .leftJoin('opd_review as o', 'a.id', 'o.admit_id')
  //     .leftJoin('opd_treatment as t', 'a.id', 't.admit_id')
  //     .leftJoin('patient_allergy as pa', 'a.id', 'pg.admit_id')
  //     .select(
  //       'a.id as admit_id', 'a.an', 'a.hn', 'a.pre_diagnosis',
  //       'p.title', 'p.fname', 'p.lname', 'p.gender',
  //       'p.age', 'p.address', 'p.phone', 'p.inscl_name',
  //       'o.chief_complaint', 'o.present_illness', 'o.past_history', 'o.physical_exam',
  //       'o.body_temperature', 'o.body_weight', 'o.body_height', 'o.waist', 'o.pulse_rate', 'o.respiratory_rate',
  //       'o.systolic_blood_pressure', 'o.diatolic_blood_pressure',
  //       'o.oxygen_sat', 'o.eye_score', 'o.movement_score', 'o.verbal_score'
  //     )
  //     .where('a.id', id);
  //   return sql
  // }

  demoTransaction(db: Knex, id: any, wardId: any, data: any) {
    return new Promise((resolve: any, reject: any) => {
      db.transaction((trx) => {
        db('admit')
          .update('ward_id', wardId)
          .where('id', id)
          .transacting(trx)
          .then(() => {
            // insert
            return db('patient_move')
              .insert(data)
              .then(trx.commit)
              .catch(trx.rollback);
          })

      }).then(() => {
        resolve();
      })
        .catch((error: any) => {
          reject(error);
        })
    })
  }
}