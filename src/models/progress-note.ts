import { Knex } from 'knex';
import { UUID } from 'crypto';

export class ProgressNoteModel {

  /**
   * 
   * @param db 
   * @param data 
   * @returns 
   */
  create(db: Knex, data: any) {
    return db('progress_note')
      .insert(data)
  }

  /**
   * 
   * @param db 
   * @param data 
   * @param progressNoteID 
   * @returns 
   */
  update(db: Knex, data: any, progressNoteID: UUID) {
    return db('progress_note')
      .where('id', progressNoteID)
      .update(data);
  }

  /**
   * 
   * @param db 
   * @param data 
   * @param doctorOrderID 
   * @returns 
   */
  updateByDoctorOrderID(db:Knex, data: object, doctorOrderID: UUID) {
    return db('progress_note')
    .where('doctor_order_id', doctorOrderID)
    .update(data);
  }

  /**
   * 
   * @param db 
   * @param progressNoteID 
   * @returns 
   */
  delete(db: Knex, progressNoteID: UUID) {
    return db('progress_note')
      .where('id', progressNoteID)
      .delete();
  }

  /**
   * 
   * @param db 
   * @param data 
   * @returns 
   */
  upsert(db: Knex, data: any) {
    return db('progress_note')
      .insert(data)
      .onConflict('doctor_order_id')
      .merge()
      .returning('*')
  }

  /**
   * 
   * @param db 
   * @param doctorOrderID 
   * @returns 
   */
  getByDoctorOrderID(db: Knex, doctorOrderID: UUID) {
    return db('progress_note')
      .where('doctor_order_id', doctorOrderID)
      .select(
        'id', 'doctor_order_id',
        'subjective', 'objective', 'assertment', 'plan', 'note'
      )
      .first()
  }
}